<x-user-layout>
    <x-slot name="header">
        {{ __('Dashboard') }}
    </x-slot>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#bs-modal-siswa">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg> &nbsp; Peminjaman Baru
                        </button>
                    
                    
                </h3>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Barang</th>
                            <th>Tanggal/Jam</th>
                            <th>Keterangan</th>
                            {{-- <th></th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($peminjaman as $data)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $data->nomor }} - {{ $data->nama_barang }}</td>
                                <td>{{ $data->created_at }}</td>
                                <td>{{ $data->keterangan }}</td>
                                {{-- <td>
                                    @if ($data->keterangan === "Barang sedang dipinjam")
                                    <form action="{{ route('peminjaman.updateKeterangan', $data->id) }}" method="post">
                                        @method('PATCH')
                                        @csrf
                                        <input type="hidden" name="data" value="Barang sudah dikembalikan">
                                        <input type="submit" class="btn btn-success btn-sm ml-1" value="Kembalikan Barang">
                                    </form>
                                    @endif
                                    
                                </td> --}}
                            </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Extra large modal -->
 
<div class="modal modal-primary fade" id="bs-modal-siswa" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel16">Peminjaman Barang</h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
      <div class="modal-body">
          <form action="{{ route('user.peminjaman.store') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group pt-1">
                        <label for="no_induk">Nomor Identitas</label>
                        <input type="hidden" name="peminjam_id" value="{{ $profile->id }}">
                        @if (Auth::user()->role === 'Guru')
                        <input type="text" name="id" value="{{ $profile->nip }}" class="form-control @error('peminjam_id') is-invalid @enderror" readonly>
                        @else
                        <input type="text" name="id" value="{{ $profile->no_induk }}" class="form-control @error('peminjam_id') is-invalid @enderror" readonly>
                            
                        @endif
                    </div>
                    <div class="form-group pt-1">
                        <label for="nama_siswa">Nama</label>
                        <input type="text" readonly id="nama" value="{{ $profile->nama }}" name="nama" class="form-control @error('nama') is-invalid @enderror">
                        <input type="hidden" name="status" value="{{ Auth::user()->role }}">
                    </div>
                    <div class="form-group pt-1">
                        <label for="jk">Nama Barang</label>
                        <select id="barang" name="barang_id" class="select2bs4 form-control @error('barang') is-invalid @enderror">
                            <option value="">-- Pilih Barang --</option>
                            @foreach ($barang as $item)
                                <option value="{{ $item->id }}" @if ($item->kondisi == 'rusak berat')
                                    disabled
                                @endif> {{ $item->nama }} ({{ $item->kondisi }})</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-bs-dismiss="modal" ><i class='nav-icon fas fa-arrow-left'></i> &nbsp; Kembali</button>
              <button type="submit" class="btn btn-primary"><i class="nav-icon fas fa-save"></i> &nbsp; Tambahkan</button>
          </form>
      </div>
      </div>
    </div>
  </div>

</x-user-layout>
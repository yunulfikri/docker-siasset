<x-user-layout>
    <x-slot name="header">
        {{ __('Dashboard') }}
    </x-slot>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    
                </h3>
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs justify-content-center" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="siswa-tab-center" data-bs-toggle="tab" href="#siswa-center"
                            aria-controls="siswa-center" role="tab" aria-selected="false">Siswa</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="guru-tab-center" data-bs-toggle="tab" href="#guru-center"
                            aria-controls="guru-center" role="tab" aria-selected="true">Guru</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="databarang-tab-center" data-bs-toggle="tab" href="#databarang-center"
                            aria-controls="databarang-center" role="tab" aria-selected="false">Data Barang</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="databarangtetap-tab-center" data-bs-toggle="tab" href="#databarangtetap-center"
                            aria-controls="databarangtetap-center" role="tab" aria-selected="true">Data Barang Tetap</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="siswa-center" aria-labelledby="siswa-tab-center" role="tabpanel">
                        <div class="card-body">
                            <div class="table-responsive">
                            <table id="peminjamantabel" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Siswa</th>
                                        <th>Barang</th>
                                        <th>Keterangan</th>
                                        <th>Waktu Peminjaman</th>
                                        <th>Waktu Pengembalian</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($peminjamansiswa as $data)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $data->nama_siswa }}</td>
                                        <td>{{ $data->nomor }} - {{ $data->nama_barang }}</td>
                                        <td class="text-capitalize">{{ $data->keterangan }}</td>
                                        <td>{{ $data->created_at }}</td>
                                        <td> @if ($data->keterangan === 'Barang sudah dikembalikan')
                                            {{ $data->updated_at }}
                                            @endif</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="guru-center" aria-labelledby="guru-tab-center" role="tabpanel">
                        <div class="card-body">
                            <div class="table-responsive">
                            <table id="peminjamantabel" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Guru</th>
                                        <th>Barang</th>
                                        <th>Keterangan</th>
                                        <th>Waktu Peminjaman</th>
                                        <th>Waktu Pengembalian</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($peminjamanguru as $data)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $data->nama_guru }}</td>
                                        <td>{{ $data->nomor }} - {{ $data->nama_barang }}</td>
                                        <td class="text-capitalize ">{{ $data->keterangan }}</td>
                                        <td>{{ $data->created_at }}</td>
                                        <td> @if ($data->keterangan === 'Barang sudah dikembalikan')
                                            {{ $data->updated_at }}
                                            @endif</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="databarang-center" aria-labelledby="databarang-tab-center" role="tabpanel">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <div class="flex">
                                        <div>
                                        </div>
                                        <div>
                                            <div class="flex items-center">
                                                <form class="flex items-center" action="{{ route('admin.barang.print')}}" method="get">
                                                    <div><select id="kondisi" name="kondisi" class="select2bs4 form-control" required>
                                                        <option value="semua" selected>Semua</option>
                                                        <option value="baik">Baik</option>
                                                        <option value="rusak ringan">Rusak Ringan</option>
                                                        <option value="rusak berat">Rusak Berat</option>
                                                    </select></div>
                                                    <div>
                                                        <input type="submit" value="Download PDF" class="btn btn-warning ml-1">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                
                                    </div>
                
                
                                </h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="barangtable" class="table table-striped table-hover table-primary">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nomor Barang</th>
                                            <th>Nama</th>
                                            <th>Merk</th>
                                            <th>Tahun</th>
                                            <th>Kondisi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($barang as $data)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $data->nomor }}</td>
                                            <td>{{ $data->nama }}</td>
                                            <td>{{ $data->merk }}</td>
                                            <td>{{ $data->tahun }}</td>
                                            <td>{{ $data->kondisi }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="databarangtetap-center" aria-labelledby="databarangtetap-tab-center" role="tabpanel">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <div class="flex">
                                        <div>
                                        </div>
                                        <div>
                                            <div class="flex items-center">
                                                <form class="flex items-center" action="{{ route('admin.barangtetap.print')}}" method="get">
                                                    <div><select id="kondisi" name="kondisi" class="select2bs4 form-control" required>
                                                        <option value="semua" selected>Semua</option>
                                                        <option value="baik">Baik</option>
                                                        <option value="rusak ringan">Rusak Ringan</option>
                                                        <option value="rusak berat">Rusak Berat</option>
                                                    </select></div>
                                                    <div>
                                                        <input type="submit" value="Download PDF" class="btn btn-warning ml-1">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                
                                    </div>
                                </h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="barangtable" class="table table-striped table-hover table-primary">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nomor Barang</th>
                                            <th>Nama</th>
                                            <th>Merk</th>
                                            <th>Tahun</th>
                                            <th>Lokasi</th>
                                            <th>Kondisi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($barangtetap as $data)
                                          <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $data->nomor }}</td>
                                        <td>{{ $data->nama }}</td>
                                        <td>{{ $data->merk }}</td>
                                        <td>{{ $data->tahun }}</td>
                                        <td>{{ $data->lokasi }}</td>
                                        <td>{{ $data->kondisi }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-user-layout>
<x-user-layout>
    <x-slot name="header">
        {{ __('My Profile') }}
    </x-slot>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                My Profile
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs justify-content-start" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="siswa-tab-center" data-bs-toggle="tab" href="#siswa-center"
                            aria-controls="siswa-center" role="tab" aria-selected="false">Akun</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="guru-tab-center" data-bs-toggle="tab" href="#guru-center"
                            aria-controls="guru-center" role="tab" aria-selected="true">Ubah Password</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="siswa-center" aria-labelledby="siswa-tab-center" role="tabpanel">
                        <div class="card-body">
                            <div style="max-width: 400px">
                            <table class="table">
                                <tr>
                                    <td>Nama</td>
                                    <td>{{ $user->name }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>{{ $user->email }}</td>
                                </tr>
                                <tr>
                                    <td>Sebagai (hak akses)</td>
                                    <td>{{ $user->role }}</td>
                                </tr>
                            </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="guru-center" aria-labelledby="guru-tab-center" role="tabpanel">
                        <div class="card-body">
                            <div class="" style="max-width: 400px">
                                <form action="{{ route('user.passchange') }}" method="post">
                                    @csrf
                                    <div class="form-group pt-1">
                                        <label for="Password">Password Baru</label>
                                        <input type="password" name="password1" id="" class="form-control">
                                    </div>
                                    <div class="form-group pt-1">
                                        <label for="Password">Konfirmasi Password Baru</label>
                                        <input type="password" name="password2" id="" class="form-control">
                                    </div>
                                    <button type="submit" class="btn btn-primary mt-1">Ubah Password</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-user-layout>
@section('pagecss')
<link rel="stylesheet" type="text/css" href="app-assets/css/pages/page-knowledge-base.css">
@endsection
@section('pagejs')
<script src="app-assets/js/scripts/pages/page-knowledge-base.js"></script>
<script>
    $('#menu_home').addClass('active');
</script>  
@endsection
<x-app-layout>
    <x-slot name="header">
        {{ __('Dashboard') }}
    </x-slot>

    <div class="content-body">
        <!-- Knowledge base Jumbotron -->
        <section id="knowledge-base-search">
            <div class="row">
                <div class="col-12">
                    <div class="card knowledge-base-bg text-center"
                        style="background-image: url('app-assets/images/banner/banner.png')">
                        <div class="card-body">
                            <h2 class="text-primary">Sistem Informasi Asset Sekolah</h2>
                            <p class="card-text mb-2">
                                <span>Popular searches: </span><span class="fw-bolder">Data Siswa, Peminjaman</span>
                            </p>
                            <form class="kb-search-input">
                                <div class="input-group input-group-merge">
                                    <span class="input-group-text"><svg xmlns="http://www.w3.org/2000/svg" width="14"
                                            height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-search">
                                            <circle cx="11" cy="11" r="8"></circle>
                                            <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                                        </svg></span>
                                    <input type="text" class="form-control" id="searchbar"
                                        placeholder="Ask a question...">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/ Knowledge base Jumbotron -->

        <!-- Knowledge base -->
        <section id="knowledge-base-content">
            <div class="row kb-search-content-info match-height">
                <!-- sales card -->
                <div class="col-md-4 col-sm-6 col-12 kb-search-content">
                    <div class="card">
                        <a href="{{ route('admin.siswa') }}">
                            <img src="app-assets/images/illustration/email.svg" class="card-img-top"
                                alt="knowledge-base-image">

                            <div class="card-body text-center">
                                <h4>Data Siswa</h4>
                                {{-- <p class="text-body mt-1 mb-0">
                                    There is perhaps no better demonstration of the folly of image of our tiny world.
                                </p> --}}
                            </div>
                        </a>
                    </div>
                </div>

                <!-- marketing -->
                <div class="col-md-4 col-sm-6 col-12 kb-search-content">
                    <div class="card">
                        <a href="{{ route('admin.guru') }}">
                            <img src="app-assets/images/illustration/marketing.svg" class="card-img-top"
                                alt="knowledge-base-image">
                            <div class="card-body text-center">
                                <h4>Data Guru</h4>
                                {{-- <p class="text-body mt-1 mb-0">
                                    Look again at that dot. That’s here. That’s home. That’s us. On it everyone you
                                    love.
                                </p> --}}
                            </div>
                        </a>
                    </div>
                </div>

                <!-- api -->
                <div class="col-md-4 col-sm-6 col-12 kb-search-content">
                    <div class="card">
                        <a href="{{ route('admin.barang') }}">
                            <img src="app-assets/images/illustration/api.svg" class="card-img-top"
                                alt="knowledge-base-image">
                            <div class="card-body text-center">
                                <h4>Data Barang</h4>
                                {{-- <p class="text-body mt-1 mb-0">every hero and coward, every creator and destroyer of
                                    civilization.</p> --}}
                            </div>
                        </a>
                    </div>
                </div>

                <!-- personalization -->
                <div class="col-md-4 col-sm-6 col-12 kb-search-content">
                    <div class="card">
                        <a href="{{ route('admin.peminjaman') }}">
                            <img src="app-assets/images/illustration/personalization.svg" class="card-img-top"
                                alt="knowledge-base-image">
                            <div class="card-body text-center">
                                <h4>Peminjaman</h4>
                                {{-- <p class="text-body mt-1 mb-0">It has been said that astronomy is a humbling and
                                    character experience.</p> --}}
                            </div>
                        </a>
                    </div>
                </div>

                

                
                <!-- no result -->
                <div class="col-12 text-center no-result no-items">
                    <h4 class="mt-4">Search result not found!!</h4>
                </div>
            </div>
        </section>
        <!-- Knowledge base ends -->
    </div>
</x-app-layout>

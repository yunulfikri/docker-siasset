@section('pagecss')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css') }}">
@endsection
@section('pagejs')
    <script src="{{ asset('app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/forms/pickers/form-pickers.min.js') }}"></script>
<script>
    $('#menu_peminjaman').addClass('active');
    function downloadPDF() {
        var dates = $('#fp-range').val()
        window.open("{{ route('admin.peminjaman.exportpdf', "dates") }}")
        // $.get("{{ route('admin.peminjaman.exportpdf') }}", { date: dates })
    }
</script>
@endsection
<x-app-layout>
    <x-slot name="header">
        {{ __('Peminjaman Barang') }}
    </x-slot>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Data peminjaman @if ($date ?? '') : {{ $date[0] }} sampai {{ $date[1] }} @endif</h4>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
                <ul class="nav nav-tabs justify-content-center" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="siswa-tab-center" data-bs-toggle="tab" href="#siswa-center"
                            aria-controls="siswa-center" role="tab" aria-selected="false">Siswa</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="guru-tab-center" data-bs-toggle="tab" href="#guru-center"
                            aria-controls="guru-center" role="tab" aria-selected="true">Guru</a>
                    </li>
                </ul>
               <div class="col-12">
                <form action="{{ route('admin.peminjaman') }}" method="get">
                    <div class="flex justify-center align-items-end">
                        <div class="form-group pt-1">
                            <label for="">Tanggal</label>
                            <input type="text" name="date" id="fp-range" @if ($date ?? '') value="{{ $date[0] ?? '' }} to {{ $date[1] ?? '' }}" @endif class="form-control flatpickr-range flatpickr-input active" style="width: 250px" placeholder="YYYY-MM-DD to YYYY-MM-DD" readonly="readonly">
                        </div>
                        <button type="submit"  value="Filter" class="btn btn-primary ml-1" style="max-height: 40px">Filter</button>
                         </form>
                        <a onclick="downloadPDF()" class="btn btn-warning ml-1">Download PDF</a>
                    </div>
               </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="siswa-center" aria-labelledby="siswa-tab-center" role="tabpanel">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="table_id" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Siswa</th>
                                            <th>Barang</th>
                                            <th>Keterangan</th>
                                            <th>Waktu Peminjaman</th>
                                            <th>Waktu Pengembalian</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($peminjamansiswa as $data)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $data->nama_siswa }}</td>
                                            <td>{{ $data->nomor }} - {{ $data->nama_barang }}</td>
                                            <td class="text-capitalize">{{ $data->keterangan }}</td>
                                            <td>{{ $data->created_at }}</td>
                                            <td>
                                                @if ($data->keterangan === 'Barang sudah dikembalikan')
                                                {{ $data->updated_at }}
                                                @endif
                                                </td>
                                            <td>
                                                <form action="{{ route('user.peminjaman.updateKeterangan', $data->id) }}" method="post">
                                                    @csrf
                                                    @method('patch')
                                                    <div class="flex">
                                                       
                                                        <button type="submit" name="data" value="Barang sudah dikembalikan" class="btn btn-success btn-sm ml-1">Selesai</button>
                                                        <button type="submit" name="data" value="dibatalkan" class="btn btn-danger btn-sm ml-1">Batalkan</button>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="guru-center" aria-labelledby="guru-tab-center" role="tabpanel">
                        <div class="card-body">
                            <div class="table-responsive">
                            <table id="table_id" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Guru</th>
                                        <th>Barang</th>
                                        <th>Keterangan</th>
                                        <th>Waktu Peminjaman</th>
                                        <th>Waktu Pengembalian</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($peminjamanguru as $data)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $data->nama_guru }}</td>
                                        <td>{{ $data->nomor }} - {{ $data->nama_barang }}</td>
                                        <td class="text-capitalize ">{{ $data->keterangan }}</td>
                                        <td>{{ $data->created_at }}</td>
                                        <td>{{ $data->updated_at }}</td>
                                        <td>
                                            <form action="{{ route('user.peminjaman.updateKeterangan', $data->id) }}" method="post">
                                                @csrf
                                                @method('patch')
                                                <div class="flex">
                                                    
                                                    <button type="submit" name="data" value="Barang sudah dikembalikan" class="btn btn-success btn-sm ml-1">Selesai</button>
                                                    <button type="submit" name="data" value="dibatalkan" class="btn btn-danger btn-sm ml-1">Batalkan</button>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</x-app-layout>
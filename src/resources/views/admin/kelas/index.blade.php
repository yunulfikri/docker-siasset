@section('pagejs')
    <script>
        function getEditKelas(id, nama_kelas){

            $('#id').val(id);
            $('#nama_kelas').val(nama_kelas);

        }
        function createKelas(){
            $('#id').val('');
        }
        $('#menu_management').addClass('sidebar-group-active');
        $('#menu_kelas').addClass('active');
    </script>
@endsection
<x-app-layout>
    <x-slot name="header">
        {{ __('Kelas Manajemen') }}
    </x-slot>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">
                  <button type="button" class="btn btn-primary btn-sm" onclick="createKelas()" data-bs-toggle="modal" data-bs-target="#datakelas">
                      <i class="nav-icon fas fa-folder-plus"></i> &nbsp; Tambah Data Kelas
                  </button>
              </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="table_id" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Kelas</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($kelas as $data)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $data->nama_kelas }}</td>
                        <td>
                            <form action="{{ route('admin.kelas.destroy', $data->id) }}" method="post">
                                {{-- <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target=".view-siswa">
                                    <i class="nav-icon fas fa-users"></i> &nbsp; View Siswa
                                  </button> --}}
                                <button type="button" class="btn btn-success btn-sm" onclick="getEditKelas({{$data->id}},'{{$data->nama_kelas}}')" data-bs-toggle="modal" data-bs-target="#datakelas">
                                    <i class="nav-icon fas fa-edit"></i> &nbsp; Edit
                                  </button>
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger btn-sm"><i class="nav-icon fas fa-trash-alt"></i> &nbsp; Hapus</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    {{-- modal kelas --}}
    <div class="modal fade text-start" id="datakelas" tabindex="-1" aria-labelledby="myModalLabel4" data-bs-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel4">Tambah kelas</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin.kelas.store') }}" method="post">
                        
                        @csrf
                        <div class="row">
                            <div class="form-group pt-1">
                                <label for="nama_siswa">Kelas</label>
                                <input type="hidden" id="id" name="id">
                                <input type="text" id="nama_kelas" name="nama_kelas" class="form-control @error('nama_kelas') is-invalid @enderror">
                            </div>
                        </div>
                        <div class="d-grid col-lg-12 col-md-12 mb-1 mb-lg-0">
                        <button type="submit" class="btn btn-primary waves-effect waves-float waves-light mt-1">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>


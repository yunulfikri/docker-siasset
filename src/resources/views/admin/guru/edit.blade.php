@section('pagejs')
    <script>
        $('#menu_management').addClass('sidebar-group-active');
        $('#menu_guru').addClass('active');
        
    </script>
@endsection
<x-app-layout>
    <x-slot name="header">
        {{ __('Edit Data Guru') }}
    </x-slot>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">&nbsp; Edit Data Guru - {{ $guru->nama_guru }}</h3>
            </div>

            <form action="{{ route('admin.guru.update', $guru->id) }}" method="post">
                @csrf
                @method('patch')
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mt-1">
                            <label for="nip">NIP</label>
                            <input type="text" id="nip" name="nip" onkeypress="return inputAngka(event)" value="{{ $guru->nip }}" class="form-control @error('nip') is-invalid @enderror" disabled>
                        </div>
                        <div class="form-group mt-1">
                            <label for="nama_guru">Nama Guru</label>
                            <input type="text" id="nama_guru" name="nama_guru" value="{{ $guru->nama }}" class="form-control @error('nama_guru') is-invalid @enderror">
                        </div>
                        <div class="form-group mt-1">
                            <label for="tmp_lahir">Tempat Lahir</label>
                            <input type="text" id="tmp_lahir" name="tmp_lahir" value="{{ $guru->tmp_lahir }}" class="form-control @error('tmp_lahir') is-invalid @enderror">
                        </div>
                        <div class="form-group mt-1">
                            <label for="telp">Nomor Telpon/HP</label>
                            <input type="text" id="telp" name="telp" onkeypress="return inputAngka(event)" value="{{ $guru->telp }}" class="form-control @error('telp') is-invalid @enderror">
                        </div>
                    </div>
                    <div class="col-md-6">
                        
                        <div class="form-group mt-1">
                            <label for="jk">Jenis Kelamin</label>
                            <select id="jk" name="jk" class="select2bs4 form-control @error('jk') is-invalid @enderror">
                                <option value="">-- Pilih Jenis Kelamin --</option>
                                <option value="L"
                                    @if ($guru->jk == 'L')
                                        selected
                                    @endif
                                >Laki-Laki</option>
                                <option value="P"
                                    @if ($guru->jk == 'P')
                                        selected
                                    @endif
                                >Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group mt-1">
                            <label for="tgl_lahir">Tanggal Lahir</label>
                            <input type="date" id="tgl_lahir" name="tgl_lahir" value="{{ $guru->tgl_lahir }}" class="form-control @error('tgl_lahir') is-invalid @enderror">
                        </div>
                        <div class="form-group mt-1">
                            <label for="kode">Kode</label>
                            <input type="text" id="kode" name="kode" class="form-control" value="{{ $guru->kode }}">
                        </div>
                        <div class="form-group pt-1">
                            <label for="email">Email</label>
                            <input type="text" id="email" name="email" value="{{ $akun->email }}" class="form-control @error('email') is-invalid @enderror" required>
                        </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
        
                <div class="card-footer">
                  <a href="{{ route('admin.guru') }}" name="kembali" class="btn btn-default" id="back"><i class='nav-icon fas fa-arrow-left'></i> &nbsp; Kembali</a> &nbsp;
                  <button name="submit" class="btn btn-primary"><i class="nav-icon fas fa-save"></i> &nbsp; Simpan</button>
                </div>
              </form>
        </div>
    </div>
</x-app-layout>
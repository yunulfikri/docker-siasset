@section('pagejs')
    <script>
        $('#menu_management').addClass('sidebar-group-active');
        $('#menu_guru').addClass('active');
        
    </script>
@endsection
<x-app-layout>
    <x-slot name="header">
        {{ __('Data Guru') }}
    </x-slot>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    <button type="button" class="btn btn-default btn-sm" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg> &nbsp; Tambah Data Guru
                    </button>
                </h3>
            </div>

            <!-- /.card-header -->
            <div class="card-body">
              <table id="table_id" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>NIP</th>
                        <th>Kode</th>
                        <th>Nama Guru</th>
                        <th>Telp</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($guru as $data)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $data->nip }}</td>
                            <td>{{ $data->kode }}</td>
                            <td>{{ $data->nama }}</td>
                            <td>{{ $data->telp }}</td>
                            <td class="text-center">
                                <form
                                        action="{{ route('admin.guru.destroy', $data->id) }}"
                                        method="post">
                                        @csrf
                                        @method('delete')
                                        <a href="{{ route('admin.guru.edit', Crypt::encrypt($data->id)) }}"
                                            class="btn btn-success btn-sm"><svg xmlns="http://www.w3.org/2000/svg"
                                                width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                stroke-linejoin="round" class="feather feather-edit-3">
                                                <path d="M12 20h9"></path>
                                                <path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z">
                                                </path>
                                            </svg> &nbsp; Edit</a>
                                        <button class="btn btn-danger btn-sm"><svg
                                                xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-trash">
                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                <path
                                                    d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                </path>
                                            </svg> &nbsp; Hapus</button>
                                    </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
        </div>
    </div>

    <!-- Extra large modal -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel4">Tambah data guru</h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
      <div class="modal-body">
          <form action="{{ route('admin.guru.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mt-1">
                        <label for="kode">Kode Guru</label>
                        <input type="text" id="kode" name="kode" placeholder="kode guru maksimal 3" minlength="2" maxlength="3"  class="form-control @error('kode') is-invalid @enderror">
                    </div>
                    <div class="form-group mt-1">
                        <label for="nip">NIP</label>
                        <input type="text" id="nip" name="nip"  class="form-control @error('nip') is-invalid @enderror">
                    </div>
                    <div class="form-group mt-1">
                        <label>Nama Guru</label>
                        <input type="text" name="nama_guru" class="form-control @error('nama_guru') is-invalid @enderror">
                    </div>
                    <div class="form-group mt-1">
                        <label for="tmp_lahir">Tempat Lahir</label>
                        <input type="text" id="tmp_lahir" name="tmp_lahir" class="form-control @error('tmp_lahir') is-invalid @enderror">
                    </div>
                    <div class="form-group mt-1">
                        <label for="tgl_lahir">Tanggal Lahir</label>
                        <input type="date" id="tgl_lahir" name="tgl_lahir" class="form-control @error('tgl_lahir') is-invalid @enderror">
                    </div>
                    
                    
                </div>
                <div class="col-md-6">
                    <div class="form-group mt-1">
                        <label for="jk">Jenis Kelamin</label>
                        <select id="jk" name="jk" class="select2bs4 form-control @error('jk') is-invalid @enderror">
                            <option value="">-- Pilih Jenis Kelamin --</option>
                            <option value="L">Laki-Laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group mt-1">
                        <label for="telp">Nomor Telpon/HP</label>
                        <input type="text" id="telp" name="telp" class="form-control @error('telp') is-invalid @enderror">
                    </div>
                    <div class="form-group pt-1">
                        <label for="email">Email</label>
                        <input type="text" id="email" name="email" class="form-control @error('email') is-invalid @enderror" required>
                    </div>
                    <div class="form-group mt-1">
                        <label for="foto">File input</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="foto" class="custom-file-input @error('foto') is-invalid @enderror" id="foto">
                                <label class="custom-file-label" for="foto">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal"><i class='nav-icon fas fa-arrow-left'></i> &nbsp; Kembali</button>
              <button type="submit" class="btn btn-primary"><i class="nav-icon fas fa-save"></i> &nbsp; Tambahkan</button>
          </form>
      </div>
      </div>
    </div>
  </div>
</x-app-layout>
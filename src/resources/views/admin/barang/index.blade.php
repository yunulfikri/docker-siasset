@section('pagejs')
<script>
    $('#menu_management').addClass('sidebar-group-active');
    $('#menu_barang').addClass('active');

</script>
@endsection
<x-app-layout>
    <x-slot name="header">
        {{ __('Data Barang') }}
    </x-slot>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    <div class="flex">
                        <div>
                            <button type="button" class="btn btn-default btn-sm" data-bs-toggle="modal"
                                data-bs-target=".bd-example-modal-lg">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-plus">
                                    <line x1="12" y1="5" x2="12" y2="19"></line>
                                    <line x1="5" y1="12" x2="19" y2="12"></line>
                                </svg> &nbsp; Tambah Data Barang
                            </button>
                        </div>
                        <div>
                            <div class="flex items-center">
                                <form class="flex items-center" action="{{ route('admin.barang.print')}}" method="get">
                                    <div><select id="kondisi" name="kondisi" class="select2bs4 form-control" required>
                                        <option value="semua" selected>Semua</option>
                                        <option value="baik">Baik</option>
                                        <option value="rusak ringan">Rusak Ringan</option>
                                        <option value="rusak berat">Rusak Berat</option>
                                    </select></div>
                                    <div>
                                        <input type="submit" value="Download PDF" class="btn btn-warning ml-1">
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>


                </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="table_id" class="table table-striped table-hover table-primary">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nomor Barang</th>
                            <th>Nama</th>
                            <th>Merk</th>
                            <th>Tahun</th>
                            <th>Lokasi</th>
                            <th>Kondisi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($barang as $data)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $data->nomor }}</td>
                            <td>{{ $data->nama }}</td>
                            <td>{{ $data->merk }}</td>
                            <td>{{ $data->tahun }}</td>
                            <td>{{ $data->lokasi }}</td>
                            <td>{{ $data->kondisi }}</td>
                            <td class="text-center">
                                <form action="{{ route('admin.barang.destroy', $data->id) }}" method="post" >
                                    @csrf
                                    @method('delete')
                                    <a href="{{ route('admin.barang.edit', Crypt::encrypt($data->id)) }}"
                                        class="btn btn-success btn-sm"><svg xmlns="http://www.w3.org/2000/svg"
                                            width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-edit-3">
                                            <path d="M12 20h9"></path>
                                            <path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z">
                                            </path>
                                        </svg> &nbsp; Edit</a>
                                    <button class="btn btn-danger btn-sm"><svg xmlns="http://www.w3.org/2000/svg"
                                            width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-trash">
                                            <polyline points="3 6 5 6 21 6"></polyline>
                                            <path
                                                d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                            </path>
                                        </svg> &nbsp; Hapus</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- Extra large modal -->
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel4">Tambah data guru</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin.barang.store')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mt-1">
                                    <label for="telp">Nomor Barang</label>
                                    <input type="text" id="nomor" name="nomor"
                                        class="form-control @error('nomor') is-invalid @enderror">
                                </div>
                                <div class="form-group mt-1">
                                    <label for="nama_guru">Nama Barang</label>
                                    <input type="text" id="nama" name="nama"
                                        class="form-control @error('nama') is-invalid @enderror" required>
                                </div>
                                <div class="form-group mt-1">
                                    <label for="nama_merk">Merk</label>
                                    <input type="text" id="merk" name="merk"
                                        class="form-control @error('merk') is-invalid @enderror">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mt-1">
                                    <label for="tahun">Tahun</label>
                                    <input type="text" id="tahun" name="tahun"
                                        class="form-control @error('tahun') is-invalid @enderror" required>
                                </div>
                                <div class="form-group mt-1">
                                    <label for="lokasi">Lokasi</label>
                                    <input type="text" id="lokasi" name="lokasi"
                                        class="form-control @error('lokasi') is-invalid @enderror" required>
                                </div>
                                <div class="form-group mt-1">
                                    <label for="jk">Kondisi</label>
                                    <select id="kondisi" name="kondisi"
                                        class="select2bs4 form-control @error('kondisi') is-invalid @enderror" required>
                                        <option value="">-- Pilih Jenis Kondisi --</option>
                                        <option value="baik">Baik</option>
                                        <option value="rusak ringan">Rusak Ringan</option>
                                        <option value="rusak berat">Rusak Berat</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i
                            class='nav-icon fas fa-arrow-left'></i> &nbsp; Kembali</button>
                    <button type="submit" class="btn btn-primary"><i class="nav-icon fas fa-save"></i> &nbsp;
                        Tambahkan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

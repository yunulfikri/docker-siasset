@section('pagejs')
<script>
    $('#menu_management').addClass('sidebar-group-active');
    $('#menu_barang').addClass('active');
</script>
@endsection
<x-app-layout>
    <x-slot name="header">
        {{ __('Edit Data Barang') }} - {{ $barang->nama }}
    </x-slot>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Edit Data Barang
                </h3>
            </div>
            <form action="{{ route('admin.barang.update', $barang->id) }}" method="post">
                @csrf
                @method('patch')
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-1">
                                <label for="telp">Nomor Barang</label>
                                <input type="text" id="nomor" name="nomor"
                                    class="form-control @error('nomor') is-invalid @enderror" value="{{ $barang->nomor }}">
                            </div>
                            <div class="form-group mt-1">
                                <label for="nama_guru">Nama Barang</label>
                                <input type="text" id="nama" name="nama"
                                    class="form-control @error('nama') is-invalid @enderror" value="{{ $barang->nama }}" required>
                            </div>
                            <div class="form-group mt-1">
                                <label for="nama_merk">Merk</label>
                                <input type="text" id="merk" name="merk"
                                    class="form-control @error('merk') is-invalid @enderror" value="{{ $barang->merk }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mt-1">
                                <label for="tahun">Tahun</label>
                                <input type="text" id="tahun" name="tahun"
                                    class="form-control @error('tahun') is-invalid @enderror" value="{{ $barang->tahun }}" required>
                            </div>
                            <div class="form-group mt-1">
                                <label for="lokasi">Lokasi</label>
                                <input type="text" id="lokasi" name="lokasi"
                                    class="form-control @error('lokasi') is-invalid @enderror" value="{{ $barang->lokasi }}" required>
                            </div>
                            <div class="form-group mt-1">
                                <label for="jk">Kondisi</label>
                                <select id="kondisi" name="kondisi"
                                    class="select2bs4 form-control @error('kondisi') is-invalid @enderror" required >
                                    <option value="">-- Pilih Jenis Kondisi --</option>
                                    <option value="baik" @if ($barang->kondisi == 'baik')
                                        selected
                                    @endif>Baik</option>
                                    <option value="rusak ringan" @if ($barang->kondisi == 'rusak ringan')
                                        selected
                                    @endif>Rusak Ringan</option>
                                    <option value="rusak berat" @if ($barang->kondisi == 'rusak berat')
                                        selected
                                    @endif>Rusak Berat</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <a href="{{ route('admin.barang')}}" name="kembali" class="btn btn-default" id="back"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg> &nbsp; Kembali</a> &nbsp;
                    <button name="submit" class="btn btn-primary"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-save"><path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"></path><polyline points="17 21 17 13 7 13 7 21"></polyline><polyline points="7 3 7 8 15 8"></polyline></svg> &nbsp; Update</button>
                  </div>
            </form>
        </div>
    </div>
</x-app-layout>
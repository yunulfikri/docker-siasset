<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Laporan Data Barang Tetap</title>
   <style>
       .table {
  width: 100%;
  margin-bottom: 1rem;
  color: #6e6b7b;
  vertical-align: middle;
  border-color: #ebe9f1; }
  th {
  text-align: left;
  text-align: -webkit-match-parent; }

thead,
tbody,
tfoot,
tr,
td,
th {
  border-color: inherit;
  border-style: solid;
  border-width: 0; }

.table > :not(caption) > * > * {
  padding: 0.72rem 2rem;
  background-color: var(--bs-table-bg);
  border-bottom-width: 1px;
  box-shadow: inset 0 0 0 9999px var(--bs-table-accent-bg); }

.table > tbody {
  vertical-align: inherit; }

.table > thead {
  vertical-align: bottom; }

.table > :not(:last-child) > :last-child > * {
  border-bottom-color: #ebe9f1; }
       .table-bordered > :not(caption) > * {
  border-width: 1px 0; }

.table-bordered > :not(caption) > * > * {
  border-width: 0 1px; }

.table-borderless > :not(caption) > * > * {
  border-bottom-width: 0; }
   </style>
    
</head>
<body>
    <div class="justify-center mb-3">
        <h4 class="">Data Barang Tetap</h4>
    </div>
    <hr>
    <div class="">
        <table id="peminjamantabel" class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Barang</th>
                    <th>Nomor</th>
                    <th>Merk</th>
                    <th>Tahun</th>
                    <th>Kondisi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($barang as $data)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $data->nama }}</td>
                    <td>{{ $data->nomor }}</td>
                    <td>{{ $data->merk }}</td>
                    <td class="text-capitalize">{{ $data->tahun }}</td>
                    <td>{{ $data->kondisi }}</td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</body>
</html>
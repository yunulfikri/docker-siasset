@section('pagejs')
    <script>
        $('#menu_management').addClass('sidebar-group-active');
        $('#menu_siswa').addClass('active');
        
    </script>
@endsection
<x-app-layout>
    <x-slot name="header">
        {{ __('Data Siswa') }}
    </x-slot>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#bs-modal-siswa">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg> &nbsp; Tambah Data Siswa
                    </button>
                </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="table_id" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Kelas</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($kelas as $data)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $data->nama_kelas }}</td>
                            <td>
                                <a href="{{ route('admin.siswa.kelas', Crypt::encrypt($data->id)) }}" class="btn btn-info btn-sm"><i class="nav-icon fas fa-search-plus"></i> &nbsp; Selengkapnya</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- Extra large modal -->
<div class="modal modal-primary fade" id="bs-modal-siswa" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel16">Tambah data siswa</h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
      <div class="modal-body">
          <form action="{{ route('admin.siswa.store')}} " method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group pt-1">
                        <label for="no_induk">Nomor Induk</label>
                        <input type="text" required id="no_induk" name="no_induk" onkeypress="return inputAngka(event)" class="form-control @error('no_induk') is-invalid @enderror">
                    </div>
                    <div class="form-group pt-1">
                        <label for="nama">Nama Siswa</label>
                        <input type="text" required id="nama" name="nama_siswa" class="form-control @error('nama') is-invalid @enderror">
                    </div>
                    <div class="form-group pt-1">
                        <label for="jk">Jenis Kelamin</label>
                        <select id="jk" name="jk" required class="select2bs4 form-control @error('jk') is-invalid @enderror">
                            <option value="">-- Pilih Jenis Kelamin --</option>
                            <option value="L">Laki-Laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group pt-1">
                        <label for="tmp_lahir">Tempat Lahir</label>
                        <input type="text" id="tmp_lahir" required name="tmp_lahir" class="form-control @error('tmp_lahir') is-invalid @enderror">
                    </div>
                    <div class="form-group pt-1">
                        <label for="foto">File input</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="foto" class="custom-file-input @error('foto') is-invalid @enderror" id="foto">
                                <label class="custom-file-label" for="foto">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group pt-1">
                        <label for="nis">NISN</label>
                        <input type="text" id="nis" name="nis" required onkeypress="return inputAngka(event)" class="form-control @error('nis') is-invalid @enderror">
                    </div>
                    <div class="form-group pt-1">
                        <label for="kelas_id">Kelas</label>
                        <select id="kelas_id" name="kelas_id" required class="select2bs4 form-control @error('kelas_id') is-invalid @enderror">
                            <option value="">-- Pilih Kelas --</option>
                            @foreach ($kelas as $data)
                                <option value="{{ $data->id }}">{{ $data->nama_kelas }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group pt-1">
                        <label for="telp">Nomor Telpon/HP</label>
                        <input type="text" id="telp" required name="telp"  class="form-control @error('telp') is-invalid @enderror">
                    </div>
                    <div class="form-group pt-1">
                        <label for="email">Email</label>
                        <input type="text" id="email" required name="email" class="form-control @error('email') is-invalid @enderror" required>
                    </div>
                    <div class="form-group pt-1">
                        <label for="tgl_lahir">Tanggal Lahir</label>
                        <input type="date" required id="tgl_lahir" name="tgl_lahir" class="form-control @error('tgl_lahir') is-invalid @enderror">
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-bs-dismiss="modal" ><i class='nav-icon fas fa-arrow-left'></i> &nbsp; Kembali</button>
              <button type="submit" class="btn btn-primary"><i class="nav-icon fas fa-save"></i> &nbsp; Tambahkan</button>
          </form>
      </div>
      </div>
    </div>
  </div>
</x-app-layout>
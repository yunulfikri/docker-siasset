@section('pagejs')
    <script  type="text/javascript">
        $('#menu_management').addClass('sidebar-group-active');
        $('#menu_siswa').addClass('active');
        
    </script>
@endsection
<x-app-layout>
    <x-slot name="header">
        {{ __('Data Siswa') }} - {{ $kelas->nama_kelas }}
    </x-slot>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <a href="{{ route('admin.siswa') }}" class="btn btn-success btn-sm"><svg
                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                        class="feather feather-arrow-left">
                        <line x1="19" y1="12" x2="5" y2="12"></line>
                        <polyline points="12 19 5 12 12 5"></polyline>
                    </svg> &nbsp; Kembali</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Siswa</th>
                            <th>No Induk</th>
                            <th>Foto</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($siswa as $data)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $data->nama }}</td>
                                <td>{{ $data->no_induk }}</td>
                                <td>
                                    <img src="{{ asset($data->foto) }}" width="130px" class="img-fluid rounded">
                                </td>
                                <td class="text-center">
                                    <form
                                        action="{{ route('admin.siswa.destroy', $data->id) }}"
                                        method="post">
                                        @csrf
                                        @method('delete')
                                        <a href="{{ route('admin.siswa.edit', Crypt::encrypt($data->id)) }}"
                                            class="btn btn-success btn-sm mt-2"><svg xmlns="http://www.w3.org/2000/svg"
                                                width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                stroke-linejoin="round" class="feather feather-edit-3">
                                                <path d="M12 20h9"></path>
                                                <path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z">
                                                </path>
                                            </svg> &nbsp; Edit</a>
                                        <button class="btn btn-danger btn-sm mt-2"><svg
                                                xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-trash">
                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                <path
                                                    d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                </path>
                                            </svg> &nbsp; Hapus</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</x-app-layout>

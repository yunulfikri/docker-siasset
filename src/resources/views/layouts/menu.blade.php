<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item me-auto"><a class="navbar-brand" href="#"><span class="brand-logo">
                        <img src="/app-assets/images/logo1.png" alt="" style="height: 20px" srcset="">
                    </span>
                    <h2 class="brand-text" style="color: #f5778b">{{ config('app.name', 'Laravel') }}
                    </h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i
                        class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i
                        class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc"
                        data-ticon="disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item" id="menu_home"><a class="d-flex align-items-center"
                    href="{{ route('dashboard') }}"><i data-feather="home"></i><span
                        class="menu-title text-truncate" data-i18n="Home">Home</span></a>
            </li>
            <li class="nav-item" id="menu_peminjaman"><a class="d-flex align-items-center"
                href="{{ route('admin.peminjaman') }}"><i data-feather="trello"></i><span
                    class="menu-title text-truncate" data-i18n="Trello">Peminjaman</span></a>
            </li>
            <li class="nav-item has-sub" id="menu_management">
                <a class="d-flex align-items-center" href="#">
                    <i data-feather="layout"></i><span class="menu-title text-truncate" data-i18n="Page Layouts">Data Management</span></a>
                <ul class="menu-content">
                    <li id="menu_guru">
                        <a class="d-flex align-items-center" href="{{ route('admin.guru') }}"><i
                                data-feather="circle"></i><span class="menu-item text-truncate"
                                data-i18n="Collapsed Menu">Data Guru</span></a>
                    </li>
                    <li id="menu_siswa">
                        <a class="d-flex align-items-center" href="{{ route('admin.siswa') }}"><i
                                data-feather="circle"></i><span class="menu-item text-truncate"
                                data-i18n="Collapsed Menu">Data Siswa</span></a>
                    </li>
                    <li id="menu_kelas"><a class="d-flex align-items-center" href="{{ route('admin.kelas') }}"><i
                                data-feather="circle"></i><span class="menu-item text-truncate"
                                data-i18n="Collapsed Menu">Data Kelas</span></a>
                    </li>
                    <li id="menu_barang"><a class="d-flex align-items-center" href="{{ route('admin.barang') }}"><i
                        data-feather="circle"></i><span class="menu-item text-truncate"
                        data-i18n="Collapsed Menu">Data Barang</span></a>
                    </li>
                    <li id="menu_barangtetap"><a class="d-flex align-items-center" href="{{ route('admin.barangtetap') }}"><i
                        data-feather="circle"></i><span class="menu-item text-truncate"
                        data-i18n="Collapsed Menu">Data Barang Tetap</span></a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- END: Main Menu-->

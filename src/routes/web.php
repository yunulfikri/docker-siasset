<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\GuruController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\BarangTetapController;
use App\Http\Controllers\PeminjamanController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::middleware(['auth','admin'])->group(function () {
    //
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
    
    Route::get('/admin/siswa', [SiswaController::class, 'index'])->name('admin.siswa');
    Route::post('/admin/siswa', [SiswaController::class, 'store'])->name('admin.siswa.store');
    Route::patch('/admin/siswa/{id}/update', [SiswaController::class, 'update'])->name('admin.siswa.update');
    Route::get('/admin/siswa/kelas/{id}', [SiswaController::class, 'kelas'])->name('admin.siswa.kelas');
    Route::get('/admin/siswa/{id}/edit', [SiswaController::class, 'edit'])->name('admin.siswa.edit');
    Route::delete('/admin/siswa/{id}/destroy', [SiswaController::class, 'destroy'])->name('admin.siswa.destroy');
    
    
    Route::get('/admin/kelas', [KelasController::class, 'index'])->name('admin.kelas');
    Route::post('/admin/kelas', [KelasController::class, 'store'])->name('admin.kelas.store');
    Route::delete('/admin/kelas/{id}', [KelasController::class, 'destroy'])->name('admin.kelas.destroy');
    
    
    Route::get('/admin/guru', [GuruController::class, 'index'])->name('admin.guru');
    Route::get('/admin/guru/{id}/edit', [GuruController::class, 'edit'])->name('admin.guru.edit');
    Route::post('/admin/guru', [GuruController::class, 'store'])->name('admin.guru.store');
    Route::delete('/admin/guru/{id}/delete', [GuruController::class, 'destroy'])->name('admin.guru.destroy');
    Route::patch('/admin/guru/{id}/update', [GuruController::class, 'update'])->name('admin.guru.update');
    
    
    Route::get('/admin/barang', [BarangController::class, 'index'])->name('admin.barang');
    Route::post('/admin/barang', [BarangController::class, 'store'])->name('admin.barang.store');
    Route::get('/admin/barang/{id}/edit', [BarangController::class, 'edit'])->name('admin.barang.edit');
    Route::patch('/admin/barang/{id}/update', [BarangController::class, 'update'])->name('admin.barang.update');
    Route::delete('/admin/barang/{id}/delete', [BarangController::class, 'destroy'])->name('admin.barang.destroy');
    Route::get('/admin/barang/print', [BarangController::class, 'print'])->name('admin.barang.print');


    // barang tetap

    Route::get('/admin/barangtetap', [BarangTetapController::class, 'index'])->name('admin.barangtetap');
    Route::post('/admin/barangtetap', [BarangTetapController::class, 'store'])->name('admin.barangtetap.store');
    Route::get('/admin/barangtetap/{id}/edit', [BarangTetapController::class, 'edit'])->name('admin.barangtetap.edit');
    Route::patch('/admin/barangtetap/{id}/update', [BarangTetapController::class, 'update'])->name('admin.barangtetap.update');
    Route::delete('/admin/barangtetap/{id}/delete', [BarangTetapController::class, 'destroy'])->name('admin.barangtetap.destroy');
    Route::get('/admin/barangtetap/print', [BarangTetapController::class, 'print'])->name('admin.barangtetap.print');
    

    Route::get('/admin/peminjaman', [PeminjamanController::class, 'index'])->name('admin.peminjaman');
    Route::patch('/admin/peminjaman/{id}/update', [PeminjamanController::class, 'updateKeterangan'])->name('user.peminjaman.updateKeterangan');
    Route::get('/admin/peminjaman/export', [PeminjamanController::class, 'exportPDF'])->name('admin.peminjaman.exportpdf');
    
});



// ROUTE FOR SISWA / GURU
Route::middleware(['auth','users'])->group(function () {
    //
    Route::get('/user', [PeminjamanController::class, 'userIndex'])->name('user.dashboard');
    Route::get('/user/peminjaman', [PeminjamanController::class, 'userIndex'])->name('user.peminjaman');
    Route::post('/user/peminjaman', [PeminjamanController::class, 'store'])->name('user.peminjaman.store');
    Route::patch('/user/peminjaman/{id}/update', [PeminjamanController::class, 'updateKeterangan'])->name('peminjaman.updateKeterangan');
    Route::get('/user/myprofile', [UserProfileController::class, 'index'])->name('user.myprofile');
    Route::post('/user/password/update', [UserProfileController::class, 'changePassword'])->name('user.passchange');
});



require __DIR__.'/auth.php';

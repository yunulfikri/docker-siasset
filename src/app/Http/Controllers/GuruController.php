<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Guru;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
class GuruController extends Controller
{
    //
    public function index()
    {
        # code...
        $guru = Guru::OrderBy('nama', 'asc')->get();
        return view('admin.guru.index', compact('guru'));
    }
    public function edit($id)
    {
        # code...
        $id = Crypt::decrypt($id);
        $guru = Guru::findorfail($id);
        $akun = User::where('nip', $guru->nip)->first();

        return view('admin.guru.edit', compact('guru','akun'));

    }
    public function store(Request $request)
    {
        # code... menyimpan data guru
        $this->validate($request, [
            'nip' => 'required',
            'nama_guru' => 'required',
            'kode' => 'required|string|unique:guru|min:2|max:3',
            'jk' => 'required'
        ]);
        if ($request->foto) {
            $foto = $request->foto;
            $new_foto = date('siHdmY') . "_" . $foto->getClientOriginalName();
            $foto->move('uploads/guru/', $new_foto);
            $nameFoto = 'uploads/guru/' . $new_foto;
        } else {
            if ($request->jk == 'L') {
                $nameFoto = 'uploads/guru/35251431012020_male.jpg';
            } else {
                $nameFoto = 'uploads/guru/23171022042020_female.jpg';
            }
        }
        User::create([
            'name' => strtolower($request->nama_guru),
            'email' => $request->email,
            'password' => Hash::make('12345678'),
            'role' => 'Guru',
            'nip' => $request->nip,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $guru = Guru::create([
            'nip' => $request->nip,
            'nama' => $request->nama_guru,
            'kode' => $request->kode,
            'jk' => $request->jk,
            'telp' => $request->telp,
            'tmp_lahir' => $request->tmp_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'foto' => $nameFoto
        ]);
        // return 'sukes';
        return redirect()->back()->with('success', 'Berhasil menambahkan data guru baru!');

    }
    public function update(Request $request, $id)
    {
        # code...
        $guru = Guru::findorfail($id);
        $guru_data = [
            'nama' => $request->nama_guru,
            'jk' => $request->jk,
            'telp' => $request->telp,
            'kode' => $request->kode,
            'tmp_lahir' => $request->tmp_lahir,
            'tgl_lahir' => $request->tgl_lahir
        ];
        $guru->update($guru_data);

        return redirect()->route('admin.guru')->with('success', 'Data guru berhasil diperbarui!');
    }
    public function destroy($id)
    {
        # code...
        $guru = Guru::findorfail($id);
        $guru->delete();
        return redirect()->route('admin.guru')->with('warning', 'Data guru berhasil dihapus! (Silahkan cek trash data guru)');

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BarangTetap;
use Illuminate\Support\Facades\Crypt;
use Barryvdh\DomPDF\Facade\Pdf;
use App;

class BarangTetapController extends Controller
{
    //

        //
        public function index()
        {
            # code... data barang
            $barang = BarangTetap::all();
            return view('admin.barangtetap.index', compact('barang'));
        }
        public function edit($id)
        {
            # code...
            $id = Crypt::decrypt($id);
            $barang = BarangTetap::findorfail($id);
            return view('admin.barangtetap.edit', compact('barang'));
    
        }
        public function store(Request $request)
        {
            # code...
            $this->validate($request, [
                'nama' => 'required',
                'kondisi' => 'required'
            ]);
            $barang = BarangTetap::create([
                'nomor' => $request->nomor,
                'nama' => $request->nama,
                'tahun' => $request->tahun,
                'merk' => $request->merk,
                'lokasi' => $request->lokasi,
                'jumlah' => $request->jumlah,
                'kondisi' => $request->kondisi,
            ]);
            return redirect()->back()->with('success', 'Berhasil menambahkan data barang baru!');
    
        }
        public function update(Request $request, $id)
        {
            # code...
            $this->validate($request, [
                'nama' => 'required',
                'kondisi' => 'required'
            ]);
            $barang_data = [
                'nomor' => $request->nomor,
                'nama' => $request->nama,
                'tahun' => $request->tahun,
                'lokasi' => $request->lokasi,
                'jumlah' => $request->jumlah,
                'merk' => $request->merk,
                'kondisi' => $request->kondisi,
            ];
    
            $barang = BarangTetap::findorfail($id);
            $barang->update($barang_data);
            return redirect()->route('admin.barangtetap')->with('success', 'Data barang berhasil diperbarui!');
            
        }
        public function destroy($id)
        {
            # code...
            $barang = BarangTetap::findorfail($id);
            $barang->delete();
            return redirect()->route('admin.barangtetap')->with('warning', 'Data barang berhasil dihapus! ');
        }
        public function print(Request $request)
        {
            if ($request->kondisi == "semua") {
                # code...
                $barang = BarangTetap::all();
    
            }else{
                $barang = BarangTetap::where('kondisi', $request->kondisi)->get();
            }
            $pdfview = view('admin.barangtetap.export', compact('barang'));
            $pdf = App::make('dompdf.wrapper');
            $invPDF = $pdf->loadHTML($pdfview);
            return $pdf->download('databarangtetap.pdf');
        }
}

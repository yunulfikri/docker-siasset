<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kelas;
use Illuminate\Support\Facades\Crypt;

class KelasController extends Controller
{
    //
    public function index()
    {
        # code...
        $kelas = Kelas::OrderBy('nama_kelas', 'asc')->get();
        return view('admin.kelas.index', compact('kelas'));
    }
    public function store(Request $request)
    {
        # code...
        Kelas::updateOrCreate(
            [
                'id' => $request->id
            ],
            [
                'nama_kelas' => $request->nama_kelas
            ]
        );
        return redirect()->back()->with('info', 'Data kelas berhasil diperbarui!');
    }
    public function destroy($id)
    {
        # code...
        $kelas = Kelas::findorfail($id);
        $kelas->delete();
        return redirect()->back()->with('warning', 'Data kelas berhasil dihapus!');
    }
    
}

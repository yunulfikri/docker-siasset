<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
class UserProfileController extends Controller
{
    //
    public function index()
    {
        # code...
        $user = Auth::user();
        return view('user.profile.index', compact('user'));
    }
    public function changePassword(Request $request)
    {
        # code...
        $this->validate($request, [
            'password1' => 'required|string|min:8',
            'password2' => 'required|string|min:8'
        ]);
        $user = Auth::user();
        if ($request->password1 != $request->password2 ) {
            # code...
            return redirect()->back()->with('error', 'Password tidak sama, silahkan coba kembali');
        }
        $user_data = [
            'password' => Hash::make($request->password2)
        ];
        $user->update($user_data);
        return redirect()->route('login')->with('success', 'User berhasil diperbarui!');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\Guru;
use App\Models\User;
use App\Models\Barang;
use App\Models\BarangTetap;
use App\Models\Peminjaman;
use Barryvdh\DomPDF\Facade\Pdf;
use App;
use Auth;

class PeminjamanController extends Controller
{
    //
    public function index(Request $request)
    {
        # code...
        if ($request->date) {
            # code...
            $date = explode(" to ", $request->date);
            $peminjamansiswa = Peminjaman::select('barang.nama as nama_barang','barang.nomor','peminjaman.*', 'siswa.nama as nama_siswa')
            ->whereNull('siswa.deleted_at')
            ->where('peminjaman.status', 'Siswa')
            ->whereBetween('peminjaman.created_at', [$date[0], $date[1]])
            ->join('siswa', 'siswa.id', 'peminjaman.peminjam_id')
            ->join('barang', 'barang.id', 'peminjaman.barang_id')->get();
            $peminjamanguru = Peminjaman::select('barang.nama as nama_barang','barang.nomor','peminjaman.*', 'guru.nama as nama_guru')
            ->whereNull('guru.deleted_at')
            ->where('peminjaman.status', 'Guru')
            ->whereBetween('peminjaman.created_at', [$date[0], $date[1]])
            ->join('guru', 'guru.id', 'peminjaman.peminjam_id')
            ->join('barang', 'barang.id', 'peminjaman.barang_id')->get();

            return view('admin.peminjaman.index', compact('peminjamanguru', 'peminjamansiswa', 'date'));
        }else{
            $peminjamansiswa = Peminjaman::select('barang.nama as nama_barang','barang.nomor','peminjaman.*', 'siswa.nama as nama_siswa')
            ->whereNull('siswa.deleted_at')
            ->where('peminjaman.status', 'Siswa')
            ->join('siswa', 'siswa.id', 'peminjaman.peminjam_id')
            ->join('barang', 'barang.id', 'peminjaman.barang_id')->get();
            $peminjamanguru = Peminjaman::select('barang.nama as nama_barang','barang.nomor','peminjaman.*', 'guru.nama as nama_guru')
            ->whereNull('guru.deleted_at')
            ->where('peminjaman.status', 'Guru')
            ->join('guru', 'guru.id', 'peminjaman.peminjam_id')
            ->join('barang', 'barang.id', 'peminjaman.barang_id')->get();
            return view('admin.peminjaman.index', compact('peminjamanguru', 'peminjamansiswa'));
        }
        
    }
    public function userIndex()
    {
        # code...
        $barang = Barang::whereNotIn('id', function ($query){
            $query->select('barang_id')->from('peminjaman')
            ->where('keterangan', 'Barang sedang dipinjam');
        })->get();
        $user = Auth::user();
        // return $peminjaman;
        if ($user->role == 'Siswa') {
            # code...
            $profile = Siswa::where('no_induk', $user->no_induk)->first();

            $peminjaman = Peminjaman::select('barang.nama as nama_barang','barang.nomor','peminjaman.*')
            ->where('peminjaman.peminjam_id', $profile->id)
            ->where('peminjaman.status', 'Siswa')

            ->join('barang', 'barang.id', 'peminjaman.barang_id')->get();

            return view('user.peminjaman.index', compact('barang','profile','peminjaman'));
        }elseif ($user->role == 'Guru') {
            # code...
            $profile = Guru::where('nip', $user->nip)->first();
            
            $peminjaman = Peminjaman::select('barang.nama as nama_barang','barang.nomor','peminjaman.*')
            ->where('peminjaman.peminjam_id', $profile->id)
            ->where('peminjaman.status', 'Guru')

            ->join('barang', 'barang.id', 'peminjaman.barang_id')->get();
            return view('user.peminjaman.index', compact('barang','profile','peminjaman'));
        }else{
            $peminjamansiswa = Peminjaman::select('barang.nama as nama_barang','barang.nomor','peminjaman.*', 'siswa.nama as nama_siswa')
            ->where('peminjaman.status', 'Siswa')
            ->whereNull('siswa.deleted_at')
            ->join('siswa', 'siswa.id', 'peminjaman.peminjam_id')
            ->join('barang', 'barang.id', 'peminjaman.barang_id')->get();
            $peminjamanguru = Peminjaman::select('barang.nama as nama_barang','barang.nomor','peminjaman.*', 'guru.nama as nama_guru')
            ->where('peminjaman.status', 'Guru')
            ->whereNull('guru.deleted_at')
            ->join('guru', 'guru.id', 'peminjaman.peminjam_id')
            ->join('barang', 'barang.id', 'peminjaman.barang_id')->get();

            $barang = Barang::all();
            $barangtetap = BarangTetap::all();
            return view('user.peminjaman.guest', compact('peminjamanguru', 'peminjamansiswa', 'barang', 'barangtetap'));

        }
    }
    public function store(Request $request){
        Peminjaman::create([
            'peminjam_id' => $request->peminjam_id,
            'status' => $request->status,
            'barang_id' => $request->barang_id,
            'keterangan' => 'Barang sedang dipinjam'
        ]);
        return redirect()->back()->with('success', 'Data pengajuan peminjaman barang berhasil!');

    }
    public function updateKeterangan(Request $request, $id)
    {
        # code...
        // return $request;
        $peminjaman = Peminjaman::findorfail($id);
        $peminjaman->update([
            'keterangan' => $request->data
        ]);
        return redirect()->back()->with('success', 'Data pengajuan peminjaman barang diperbaruhi!');

    }
    public function exportPDF(Request $request)
    {
        # code...
        // return "hehe";
        
        if ($request->date) {
            # code...
            $date = explode(" to ", $request->date);
            $peminjamansiswa = Peminjaman::select('barang.nama as nama_barang','barang.nomor','peminjaman.*', 'siswa.nama as nama_siswa')
            ->whereNull('siswa.deleted_at')
            ->whereBetween('peminjaman.created_at', [$date[0], $date[1]])
            ->join('siswa', 'siswa.id', 'peminjaman.peminjam_id')
            ->join('barang', 'barang.id', 'peminjaman.barang_id')->get();
            $peminjamanguru = Peminjaman::select('barang.nama as nama_barang','barang.nomor','peminjaman.*', 'guru.nama as nama_guru')
            ->whereNull('guru.deleted_at')
            ->whereBetween('peminjaman.created_at', [$date[0], $date[1]])
            ->join('guru', 'guru.id', 'peminjaman.peminjam_id')
            ->join('barang', 'barang.id', 'peminjaman.barang_id')->get();

            // $pdf = PDF::loadView('admin.peminjaman.export', $peminjamanguru);
            $pdfview = view('admin.peminjaman.export', compact('peminjamanguru', 'peminjamansiswa', 'date'));
            $pdf = App::make('dompdf.wrapper');
$invPDF = $pdf->loadHTML($pdfview);
return $pdf->download('invoice.pdf');
        }else{
            $peminjamansiswa = Peminjaman::select('barang.nama as nama_barang','barang.nomor','peminjaman.*', 'siswa.nama as nama_siswa')
            ->whereNull('siswa.deleted_at')
            ->join('siswa', 'siswa.id', 'peminjaman.peminjam_id')
            ->join('barang', 'barang.id', 'peminjaman.barang_id')->get();
            $peminjamanguru = Peminjaman::select('barang.nama as nama_barang','barang.nomor','peminjaman.*', 'guru.nama as nama_guru')
            ->whereNull('guru.deleted_at')
            ->join('guru', 'guru.id', 'peminjaman.peminjam_id')
            ->join('barang', 'barang.id', 'peminjaman.barang_id')->get();

            $pdfview = view('admin.peminjaman.export', compact('peminjamanguru', 'peminjamansiswa'));
            $pdf = App::make('dompdf.wrapper');
$invPDF = $pdf->loadHTML($pdfview);
return $pdf->download('invoice.pdf');
        }
    }
}

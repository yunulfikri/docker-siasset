<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Barang;
use Illuminate\Support\Facades\Crypt;
use Barryvdh\DomPDF\Facade\Pdf;
use App;

class BarangController extends Controller
{
    //
    public function index()
    {
        # code... data barang
        $barang = Barang::all();
        return view('admin.barang.index', compact('barang'));
    }
    public function edit($id)
    {
        # code...
        $id = Crypt::decrypt($id);
        $barang = Barang::findorfail($id);
        return view('admin.barang.edit', compact('barang'));

    }
    public function store(Request $request)
    {
        # code...
        $this->validate($request, [
            'nama' => 'required',
            'kondisi' => 'required'
        ]);
        $barang = Barang::create([
            'nomor' => $request->nomor,
            'nama' => $request->nama,
            'tahun' => $request->tahun,
            'lokasi' => $request->lokasi,
            'merk' => $request->merk,
            'kondisi' => $request->kondisi,
        ]);
        return redirect()->back()->with('success', 'Berhasil menambahkan data barang baru!');

    }
    public function update(Request $request, $id)
    {
        # code...
        $this->validate($request, [
            'nama' => 'required',
            'kondisi' => 'required'
        ]);
        $barang_data = [
            'nomor' => $request->nomor,
            'nama' => $request->nama,
            'tahun' => $request->tahun,
            'merk' => $request->merk,
            'lokasi' => $request->lokasi,
            'kondisi' => $request->kondisi,
        ];

        $barang = Barang::findorfail($id);
        $barang->update($barang_data);
        return redirect()->route('admin.barang')->with('success', 'Data barang berhasil diperbarui!');
        
    }
    public function destroy($id)
    {
        # code...
        $barang = Barang::findorfail($id);
        $barang->delete();
        return redirect()->route('admin.barang')->with('warning', 'Data barang berhasil dihapus! ');
    }

    public function print(Request $request)
    {
        if ($request->kondisi == "semua") {
            # code...
            $barang = Barang::all();

        }else{
            $barang = Barang::where('kondisi', $request->kondisi)->get();
        }
        $pdfview = view('admin.barang.export', compact('barang'));
        $pdf = App::make('dompdf.wrapper');
        $invPDF = $pdf->loadHTML($pdfview);
        return $pdf->download('databarang.pdf');
    }
}

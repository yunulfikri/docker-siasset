<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

class SiswaController extends Controller
{
    //
    public function index()
    {
        # code...untuk menampilkan data siswa
        $kelas = Kelas::OrderBy('nama_kelas', 'asc')->get();
        return view('admin.siswa.index', compact('kelas'));
    }
    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $siswa = Siswa::findorfail($id);
        $akun = User::where('no_induk', $siswa->no_induk)->first();
        // return $akun;
        $kelas = Kelas::all();
        return view('admin.siswa.edit', compact('siswa', 'kelas', 'akun'));
    }
    public function store(Request $request)
    {
        # code... untuk masukan data siswa

        $this->validate($request, [
            'no_induk' => 'required|string|unique:siswa',
            'email' => 'required|unique:users',
            'nama_siswa' => 'required',
            'jk' => 'required',
            'kelas_id' => 'required'
        ]);

        if ($request->foto) {
            $foto = $request->foto;
            $new_foto = date('siHdmY') . "_" . $foto->getClientOriginalName();
            $foto->move('uploads/siswa/', $new_foto);
            $nameFoto = 'uploads/siswa/' . $new_foto;
        } else {
            if ($request->jk == 'L') {
                $nameFoto = 'uploads/siswa/52471919042020_male.jpg';
            } else {
                $nameFoto = 'uploads/siswa/52471919042020_female.jpg';
            }
        }
        User::create([
            'name' => strtolower($request->nama_siswa),
            'email' => $request->email,
            'password' => Hash::make('12345678'),
            'role' => 'Siswa',
            'no_induk' => $request->no_induk,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Siswa::create([
            'no_induk' => $request->no_induk,
            'nis' => $request->nis,
            'nama' => $request->nama_siswa,
            'jk' => $request->jk,
            'kelas_id' => $request->kelas_id,
            'telp' => $request->telp,
            'tmp_lahir' => $request->tmp_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'foto' => $nameFoto
        ]);
        return redirect()->back()->with('success', 'Berhasil menambahkan data siswa baru!');
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_siswa' => 'required',
            'jk' => 'required',
            'kelas_id' => 'required',
        ]);

        $siswa = Siswa::findorfail($id);
        // $user = User::where('no_induk', $siswa->no_induk)->first();
        // if ($user) {
        //     $user_data = [
        //         'name' => $request->nama_siswa
        //     ];
        //     $user->update($user_data);
        // } else {
        // }
        $siswa_data = [
            'nis' => $request->nis,
            'nama' => $request->nama_siswa,
            'jk' => $request->jk,
            'kelas_id' => $request->kelas_id,
            'telp' => $request->telp,
            'tmp_lahir' => $request->tmp_lahir,
            'tgl_lahir' => $request->tgl_lahir,
        ];
        $siswa->update($siswa_data);

        return redirect()->route('admin.siswa')->with('success', 'Data siswa berhasil diperbarui!');
    }
    public function kelas($id)
    {
        $id = Crypt::decrypt($id);
        $siswa = Siswa::where('kelas_id', $id)->OrderBy('nama', 'asc')->get();
        $kelas = Kelas::findorfail($id);
        return view('admin.siswa.show', compact('siswa', 'kelas'));
    }
    public function destroy($id)
    {
        $siswa = Siswa::findorfail($id);
        $user = User::where('no_induk', $siswa->no_induk)->delete();
        $siswa->delete();
        return redirect()->back()->with('warning', 'Data siswa berhasil dihapus! (Silahkan cek trash data siswa)');
    }

}

<?php

namespace App\Models;
use Auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Siswa extends Model
{
    use SoftDeletes;
    
    use HasFactory;
    protected $table = 'siswa';
    protected $fillable = ['no_induk', 'nis', 'nama', 'kelas_id', 'jk', 'telp', 'tmp_lahir', 'tgl_lahir', 'foto'];
 

    public function kelas()
    {
        return $this->belongsTo('App\Models\Kelas')->withDefault();
    }
    public function getName($value)
    {
        return $this->attributes['nama_siswa'];
    }
}

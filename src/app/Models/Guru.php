<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guru extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['nip', 'nama', 'kode', 'jk', 'telp', 'tmp_lahir', 'tgl_lahir', 'foto'];

    protected $table = 'guru';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BarangTetap extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'barang_tetaps';

    protected $fillable = [
        'nama','nomor','kondisi','merk','tahun','lokasi','jumlah'
    ];
}

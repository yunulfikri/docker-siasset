<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Peminjaman extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'peminjaman';

    protected $fillable = [
        'peminjam_id','status','barang_id','keterangan'
    ];
}
